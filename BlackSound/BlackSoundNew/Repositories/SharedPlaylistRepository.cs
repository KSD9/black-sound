﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlackSoundNew.Entities;
using System.IO;

namespace BlackSoundNew.Repositories
{
    class SharedPlaylistRepository : BaseRepository<SharedPlaylist>
    {

        public SharedPlaylistRepository(string filePath) : base(filePath)
        {
        }

        public SharedPlaylist GetPlayListUserId(int playlistId, int UserId)
        {
            List<SharedPlaylist> result = new List<SharedPlaylist>();
            FileStream fs = new FileStream(this.filePath, FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(fs);
            try
            {
                while (!sr.EndOfStream)
                {
                    SharedPlaylist entity = new SharedPlaylist();
                    ReadEntity(entity, sr);

                    if (entity.PlayListId == playlistId && entity.UserId == UserId)
                    {
                        return entity;


                    }
                }
            }



            finally
            {
                sr.Close();
                fs.Close();
            }

            return null;
        }



        public SharedPlaylist getSharedUser(int userId)

        {
            List<SharedPlaylist> result = new List<SharedPlaylist>();
            FileStream fs = new FileStream(this.filePath, FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(fs);
            try
            {
                while (!sr.EndOfStream)
                {
                    SharedPlaylist entity = new SharedPlaylist();
                    ReadEntity(entity, sr);

                    if ( entity.UserId == userId)
                    {
                        return entity;


                    }
                }
            }



            finally
            {
                sr.Close();
                fs.Close();
            }

            return null;
        } 
        public List<SharedPlaylist> GetUserId(int UserId)
        {
            List<SharedPlaylist> result = new List<SharedPlaylist>();
            FileStream fs = new FileStream(this.filePath, FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(fs);

            try
            {
                while (!sr.EndOfStream)
                {
                    SharedPlaylist entity = new SharedPlaylist();
                    ReadEntity(entity, sr);

                    if (entity.UserId == UserId)
                    {
                        result.Add(entity);


                    }
                }
            }
            finally
            {
                sr.Close();
                fs.Close();
            }

            return result;
        }



        public List<SharedPlaylist> GetPlaylistsId(int playlistId)
        {
            List<SharedPlaylist> result = new List<SharedPlaylist>();
            FileStream fs = new FileStream(this.filePath, FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(fs);

            try
            {
                while (!sr.EndOfStream)
                {
                    SharedPlaylist entity = new SharedPlaylist();
                    ReadEntity(entity, sr);

                    if (entity.PlayListId == playlistId)
                    {
                        result.Add(entity);


                    }
                }
            }
            finally
            {
                sr.Close();
                fs.Close();
            }

            return result;
        }

        protected override void ReadEntity(SharedPlaylist entity, StreamReader sr)
        {
            entity.Id = Convert.ToInt32(sr.ReadLine());
            entity.PlayListId = Convert.ToInt32(sr.ReadLine());
            entity.UserId = Convert.ToInt32(sr.ReadLine());
        }

        protected override void WriteEntity(SharedPlaylist entity, StreamWriter sw)
        {
            sw.WriteLine(entity.Id);
            sw.WriteLine(entity.PlayListId);
            sw.WriteLine(entity.UserId);
        }
    }
}
