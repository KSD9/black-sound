﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlackSoundNew.Entities;
using System.IO;

namespace BlackSoundNew.Repositories
{
   
        abstract class BaseRepository<T> where T : BaseEntity, new()
        {
            protected readonly string filePath;

            public BaseRepository(string filePath)
            {
                this.filePath = filePath;
            }

            protected abstract void ReadEntity(T entity, StreamReader sr);
            protected abstract void WriteEntity(T entity, StreamWriter sw);

            public int GetNextId()
            {
                FileStream fs = new FileStream(this.filePath, FileMode.OpenOrCreate);
                StreamReader sr = new StreamReader(fs);

                int id = 1;
                try
                {
                    while (!sr.EndOfStream)
                    {
                        T entity = new T();
                        ReadEntity(entity, sr);

                        if (id <= entity.Id)
                        {
                            id = entity.Id + 1;
                        }
                    }
                }
                finally
                {
                    sr.Close();
                    fs.Close();
                }

                return id;
            }

            public T GetById(int id)
            {
                FileStream fs = new FileStream(this.filePath, FileMode.OpenOrCreate);
                StreamReader sr = new StreamReader(fs);

                try
                {
                    while (!sr.EndOfStream)
                    {
                        T entity = new T();
                        ReadEntity(entity, sr);

                        if (entity.Id == id)
                        {
                            return entity;
                        }
                    }
                }
                finally
                {
                    sr.Close();
                    fs.Close();
                }

                return null;
            }

            public List<T> GetAll()
            {
                List<T> result = new List<T>();

                FileStream fs = new FileStream(this.filePath, FileMode.OpenOrCreate);
                StreamReader sr = new StreamReader(fs);

                try
                {
                    while (!sr.EndOfStream)
                    {
                        T entity = new T();
                        ReadEntity(entity, sr);

                        result.Add(entity);
                    }
                }
                finally
                {
                    sr.Close();
                    fs.Close();
                }

                return result;
            }

            private void Insert(T entity)
            {
                entity.Id = GetNextId();

                FileStream fs = new FileStream(filePath, FileMode.Append);
                StreamWriter sw = new StreamWriter(fs);

                try
                {
                    WriteEntity(entity, sw);
                }
                finally
                {
                    sw.Close();
                    fs.Close();
                }
            }

            private void Update(T entity)
            {
                string tempFilePath = filePath + ".temp";

                FileStream ifs = new FileStream(filePath, FileMode.OpenOrCreate);
                StreamReader sr = new StreamReader(ifs);

                FileStream ofs = new FileStream(tempFilePath, FileMode.OpenOrCreate);
                StreamWriter sw = new StreamWriter(ofs);

                try
                {
                    while (!sr.EndOfStream)
                    {

                        T current = new T();
                        ReadEntity(current, sr);

                        if (current.Id != entity.Id)
                        {
                            WriteEntity(current, sw);
                        }
                        else
                        {
                            WriteEntity(entity, sw);
                        }


                    }
                }
                finally
                {
                    sw.Close();
                    ofs.Close();
                    sr.Close();
                    ifs.Close();
                }

                File.Delete(filePath);
                File.Move(tempFilePath, filePath);
            }

            public void Delete(T entity)
            {
                string tempFilePath = filePath + ".temp";

                FileStream ifs = new FileStream(filePath, FileMode.OpenOrCreate);
                StreamReader sr = new StreamReader(ifs);

                FileStream ofs = new FileStream(tempFilePath, FileMode.OpenOrCreate);
                StreamWriter sw = new StreamWriter(ofs);

                try
                {
                    while (!sr.EndOfStream)
                    {
                        T current = new T();
                        ReadEntity(current, sr);

                        if (current.Id != entity.Id)
                        {
                            WriteEntity(current, sw);
                        }
                    }
                }
                finally
                {
                    sw.Close();
                    ofs.Close();
                    sr.Close();
                    ifs.Close();
                }

                File.Delete(filePath);
                File.Move(tempFilePath, filePath);
            }

            public void Save(T entity)
            {
                if (entity.Id > 0)
                {
                    Update(entity);
                }
                else
                {
                    Insert(entity);
                }
            }
        }
}
