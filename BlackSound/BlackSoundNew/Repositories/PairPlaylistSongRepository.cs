﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlackSoundNew.Entities;
using System.IO;

namespace BlackSoundNew.Repositories
{
    class PairPlaylistSongRepository : BaseRepository<PlayListSongs>
    {
        public PairPlaylistSongRepository(string filePath) : base(filePath)
        {
        }

        public PlayListSongs GetPlayListSongId(int playlistId, int songId)
        {
            List<PlayListSongs> result = new List<PlayListSongs>();
            FileStream fs = new FileStream(this.filePath, FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(fs);
            try
            {
                while (!sr.EndOfStream)
                {
                    PlayListSongs entity = new PlayListSongs();
                    ReadEntity(entity, sr);

                    if (entity.PlayListId == playlistId && entity.SongId == songId)
                    {
                        return entity;


                    }
                }
            }



            finally
            {
                sr.Close();
                fs.Close();
            }

            return null;
        }

        public PlayListSongs DeletingSongById(int songId)

        {
            List<PlayListSongs> result = new List<PlayListSongs>();
            FileStream fs = new FileStream(this.filePath, FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(fs);
            try
            {
                while (!sr.EndOfStream)
                {
                    PlayListSongs entity = new PlayListSongs();
                    ReadEntity(entity, sr);

                    if (entity.SongId == songId)
                    {
                        return entity;


                    }
                }
            }



            finally
            {
                sr.Close();
                fs.Close();
            }

            return null;
        }

        public List<PlayListSongs> GetSondId(int songID)
        {
            List<PlayListSongs> result = new List<PlayListSongs>();
            FileStream fs = new FileStream(this.filePath, FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(fs);

            try
            {
                while (!sr.EndOfStream)
                {
                    PlayListSongs entity = new PlayListSongs();
                    ReadEntity(entity, sr);

                    if (entity.SongId == songID)
                    {
                        result.Add(entity);


                    }
                }
            }
            finally
            {
                sr.Close();
                fs.Close();
            }

            return result;
        }



        public List<PlayListSongs> GetSongPlaylistID(int playlistId)
        {
            List<PlayListSongs> result = new List<PlayListSongs>();
            FileStream fs = new FileStream(this.filePath, FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(fs);

            try
            {
                while (!sr.EndOfStream)
                {
                    PlayListSongs entity = new PlayListSongs();
                    ReadEntity(entity, sr);

                    if (entity.PlayListId == playlistId)
                    {
                        result.Add(entity);


                    }
                }
            }
            finally
            {
                sr.Close();
                fs.Close();
            }

            return result;
        }


        protected override void ReadEntity(PlayListSongs entity, StreamReader sr)
        {
            entity.Id = Convert.ToInt32(sr.ReadLine());
            entity.PlayListId = Convert.ToInt32(sr.ReadLine());
            entity.SongId = Convert.ToInt32(sr.ReadLine());
        }

        protected override void WriteEntity(PlayListSongs entity, StreamWriter sw)
        {
            sw.WriteLine(entity.Id);
            sw.WriteLine(entity.PlayListId);
            sw.WriteLine(entity.SongId);
        }
    }

}

