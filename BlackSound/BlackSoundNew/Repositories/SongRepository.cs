﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlackSoundNew.Entities;
using System.IO;

namespace BlackSoundNew.Repositories
{
    class SongRepository:BaseRepository <Song>
    {
        public SongRepository(string filePath) : base(filePath)
        {
        }

        public List<Song> GetAll(int songId)
        {
            List<Song> result = new List<Song>();

            FileStream fs = new FileStream(this.filePath, FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(fs);

            try
            {
                while (!sr.EndOfStream)
                {
                    Song song = new Song();
                    song.Id = Convert.ToInt32(sr.ReadLine());
                    song.Title = sr.ReadLine();
                    song.ArtistName = sr.ReadLine();
                    song.Year = Convert.ToInt32(sr.ReadLine());

                    if (song.Id== songId)
                    {
                        result.Add(song);
                    }
                }
            }
            finally
            {
                sr.Close();
                fs.Close();
            }

            return result;
        }

        protected override void ReadEntity(Song entity, StreamReader sr)
        {
            entity.Id = Convert.ToInt32(sr.ReadLine());
            entity.Title = sr.ReadLine();
            entity.ArtistName = sr.ReadLine();
            entity.Year = Convert.ToInt32(sr.ReadLine());
        }

        protected override void WriteEntity(Song entity, StreamWriter sw)
        {
            sw.WriteLine(entity.Id);
            sw.WriteLine(entity.Title);
            sw.WriteLine(entity.ArtistName);
            sw.WriteLine(entity.Year);
        }

    }
}
