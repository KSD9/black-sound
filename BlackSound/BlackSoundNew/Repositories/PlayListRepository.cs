﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlackSoundNew.Entities;
using System.IO;

namespace BlackSoundNew.Repositories
{
    class PlayListRepository:BaseRepository<PlayList>
    {

        public PlayListRepository(string filePath) : base(filePath)
        {

        }

        public List<PlayList> GetAll(int CreatedByUserId)
        {
            List<PlayList> result = new List<PlayList>();

            FileStream fs = new FileStream(this.filePath, FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(fs);

            try
            {
                while (!sr.EndOfStream)
                {
                    PlayList entity = new PlayList();
                    ReadEntity(entity, sr);
                    if (entity.CreatedByUserId == CreatedByUserId || entity.IsPublic)
                        {
                        result.Add(entity);
                    }
                }
            }
            finally
            {
                sr.Close();
                fs.Close();
            }

            return result;
        }

        protected override void ReadEntity(PlayList entity, StreamReader sr)
        {

            entity.Id = Convert.ToInt32(sr.ReadLine());
            entity.Name = sr.ReadLine();
            entity.Description = sr.ReadLine();
            entity.IsPublic = Convert.ToBoolean(sr.ReadLine());
            entity.CreatedByUserId = Convert.ToInt32(sr.ReadLine());
        }

        protected override void WriteEntity(PlayList entity, StreamWriter sw)
        {
            sw.WriteLine(entity.Id);
            sw.WriteLine(entity.Name);
            sw.WriteLine(entity.Description);
            sw.WriteLine(entity.IsPublic);
            sw.WriteLine(entity.CreatedByUserId);
        }

     
    }
}
