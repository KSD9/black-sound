﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlackSoundNew.Entities;

using System.IO;

namespace BlackSoundNew.Repositories
{
    class UserRepository : BaseRepository<User>
    {

        public UserRepository(string filePath) : base(filePath)
        {
        }

        public User GetByUsernameAndPassword(string username, string password)
        {
            FileStream fs = new FileStream(this.filePath, FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(fs);

            try
            {
                while (!sr.EndOfStream)
                {
                    User entity = new User();
                    entity.Id = Convert.ToInt32(sr.ReadLine());
                    entity.Email = sr.ReadLine();
                    entity.Password = sr.ReadLine();
                    entity.DisplayName = sr.ReadLine();
                    entity.IsAdministrator = Convert.ToBoolean(sr.ReadLine());

                    if (entity.Email == username && entity.Password == password)
                    {
                        return entity;
                    }
                }
            }
            finally
            {
                sr.Close();
                fs.Close();
            }

            return null;
        }

        protected override void ReadEntity(User entity, StreamReader sr)
        {
            entity.Id = Convert.ToInt32(sr.ReadLine());
            entity.Email = sr.ReadLine();
            entity.Password = sr.ReadLine();
            entity.DisplayName = sr.ReadLine();
            entity.IsAdministrator = Convert.ToBoolean(sr.ReadLine());
        }

        protected override void WriteEntity(User entity, StreamWriter sw)
        {
            sw.WriteLine(entity.Id);
            sw.WriteLine(entity.Email);
            sw.WriteLine(entity.Password);
            sw.WriteLine(entity.DisplayName);
            sw.WriteLine(entity.IsAdministrator);
        }

    }


}





