﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlackSoundNew.Entities;
using BlackSoundNew.Repositories;
using BlackSoundNew.Services;
using BlackSoundNew.Tools;
using System.IO;
using System.Reflection;

namespace BlackSoundNew.ViewControllers
{
    abstract class BaseView<T> where T : BaseEntity, new()
    {
        private List<T> items;
        public BaseView(List<T> items)
        {
            this.items = items;
        }
        public abstract BaseRepository<T> GetRepository();






        public void ViewAll()

        {
            List<T> results = GetRepository().GetAll();
            Console.WriteLine(typeof(T).Name + "s");
            foreach (var item in items)
            {
                foreach (var pi in item.GetType().GetProperties())
                {
                    Console.WriteLine($"{pi.Name}: {pi.GetValue(item)}");
                }
                Console.WriteLine("========================");

            }
            Console.ReadKey(true);
        }

        public void Get(int id)
        {
            T item = items.SingleOrDefault(i => i.Id == id);
            foreach (var pi in item.GetType().GetProperties())
            {
                Console.WriteLine($"{pi.Name}: {pi.GetValue(item)}");
            }
            Console.WriteLine("========================");
            Console.ReadKey();
        }

        public void Insert()

        {
            try
            {
                T item = new T();
                foreach (var pi in item.GetType().GetProperties())
                {
                    if (pi.Name != "Id")
                    {
                        Console.Write("\n Write " + pi.Name + ": ");
                        var value = Console.ReadLine();


                        if (value == null || value.Equals(""))
                        {
                            Console.WriteLine("Field cannot be blank!");
                            Console.ReadKey();
                            return;
                        }
                        else
                        {
                            pi.SetValue(item, Convert.ChangeType(value, pi.PropertyType), null);
                        }
                    }
                }


                GetRepository().Save(item);
                Console.WriteLine("Song Added Succesfuly");
                Console.ReadKey(true);
            }
            catch (Exception)
            { Console.WriteLine("Enter Valid Data");
                Console.ReadKey(true);
                return;
            }
        }


        public void Update()
        {
            try
            {



                T item = new T();
                PropertyInfo prop = item.GetType().GetProperty("Id");
                Console.Write("\n Write :{0}", prop.Name);
                int id = int.Parse(Console.ReadLine());
                var repository = GetRepository();
                item = repository.GetById(id);

                if (item == null)
                {
                    Console.WriteLine("The Song Does Not Exist!");
                    Console.ReadKey(true);
                    return;
                }

                foreach (var pi in item.GetType().GetProperties())
                {

                    if (pi.Name != "Id")
                    {
                        Console.Write($"{pi.Name}: ");
                        var value = Console.ReadLine();

                        if (value == null || value.Equals(""))
                        {
                            Console.WriteLine("Field cannot be blank!");
                            Console.ReadKey();
                            return;
                        }
                        else
                        {

                        
                            pi.SetValue(item, Convert.ChangeType(value, pi.PropertyType), null);
                        }
                    }



                }

                GetRepository().Save(item);
                Console.WriteLine("Song Updated Succesfuly");
                Console.ReadKey(true);

            }
            catch (Exception)
            {

                Console.WriteLine("Enter Valid data!");
                Console.ReadKey(true);
                return;
            }
        }

        public int Delete()
        {
            T item = new T();
            PropertyInfo pi = item.GetType().GetProperty("Id");
            Console.Write("\n Write " + pi.Name + ": ");
            int id = int.Parse(Console.ReadLine());
            var repository = GetRepository();
            item = repository.GetById(id);
            if (item == null)
            {
                Console.WriteLine("The Song Does Not Exist!");
                Console.ReadKey(true);

            }

            repository.Delete(item);
            return id;

        }
    }
}

