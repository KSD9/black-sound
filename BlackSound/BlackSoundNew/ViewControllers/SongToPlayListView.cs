﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlackSoundNew.Entities;
using BlackSoundNew.Repositories;

namespace BlackSoundNew.ViewControllers
{
    class SongToPlayListView
    {
        public void Add()
        {
            Console.Clear();

            PlayListSongs playListSong = new PlayListSongs();

            try { 
            Console.WriteLine("Add Song:");

            Console.Write("PlayList ID: ");
            playListSong.PlayListId = int.Parse(Console.ReadLine());

            Console.Write("Song ID: ");
            playListSong.SongId = int.Parse(Console.ReadLine());



            PairPlaylistSongRepository songPlaylist = new PairPlaylistSongRepository(@"../../DataFiles/PairPlaylistSong.txt");
            songPlaylist.Save(playListSong);

            Console.WriteLine("Song Added successfully.");
            Console.ReadKey(true);
            }
            catch (Exception)
            {
                Console.WriteLine("Enter valid data ! ");
                Console.ReadKey(true);
                return;
            }
        }

        public void ViewSongs()
        {
            PlayListRepository playlists = new PlayListRepository(@"../../DataFiles/PlayLists.txt");
            PairPlaylistSongRepository songPlaylist = new PairPlaylistSongRepository(@"../../DataFiles/PairPlaylistSong.txt");
            SongRepository songs = new SongRepository(@"../../DataFiles/Songs.txt");
            List<PlayList> playlist = playlists.GetAll();
            PlayListSongs playListSong = new PlayListSongs();
            int playlistId = int.Parse(Console.ReadLine());
            PlayListView playlistView = new PlayListView(playlist);
        }

    }
}

