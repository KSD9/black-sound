﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlackSoundNew.Entities;
using BlackSoundNew.Repositories;

namespace BlackSoundNew.ViewControllers
{
    class UserView
    {

        public void Add()
        {
            Console.Clear();

            User user = new User();

            Console.WriteLine("Add new User:");

            Console.Write("Email: ");
            user.Email = Console.ReadLine();

            Console.Write("Password: ");
            user.Password = Console.ReadLine();

            Console.Write("Display Name: ");
            user.DisplayName = Console.ReadLine();

            Console.Write("Is Admin (True/False): ");
            user.IsAdministrator = Convert.ToBoolean(Console.ReadLine());

            UserRepository usersRepository = new UserRepository(@"../../DataFiles/users.txt");
            usersRepository.Save(user);

            Console.WriteLine("User saved successfully.");
            Console.ReadKey(true);
        }
    }
}
