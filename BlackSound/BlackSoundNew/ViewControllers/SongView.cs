﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlackSoundNew.Entities;
using BlackSoundNew.Repositories;
using BlackSoundNew.Tools;
using BlackSoundNew.Services;

namespace BlackSoundNew.ViewControllers
{
    class SongView : BaseView<Song>

    {
        public SongView(List<Song> items) : base(items)
        {

        }

        public override BaseRepository<Song> GetRepository()
        {
            return new SongRepository(@"../../DataFiles/Songs.txt");
        }

        private void AllSongsPlaylist() {

            PlayListRepository playlists = new PlayListRepository(@"../../DataFiles/PlayLists.txt");
            PlayList entity = playlists.GetById(1);
            SinglePlayListView AllSongsPlayList = new SinglePlayListView(entity);
            AllSongsPlayList.Show();

        }
        public void Show()
        {
            while (true)
            {
                SongMenu choice = RenderMenu();

                switch (choice)
                {
                    case SongMenu.Select:
                        {
                            ViewAll();
                            break;
                        }
                    case SongMenu.Insert:
                        {
                            
                            Add();

                            break;
                        }
                    case SongMenu.Update:
                        {
                            Update();
                            break;
                        }
                    case SongMenu.Delete:
                        {

                            DeleteFromPlayLists();
                            break;
                        }
                    case SongMenu.View:
                        {
                            AllSongsPlaylist();
                            break;
                        }
                    case SongMenu.Exit:
                        {
                            return;
                        }
                }
            }
        }

        private SongMenu RenderMenu()
        {
            while (true)
            {
                Console.Clear();

                Console.WriteLine("##########################");
                Console.WriteLine("[G]et all Songs");
                Console.WriteLine("[A]dd Song");
                Console.WriteLine("[U]pdate Song");
                Console.WriteLine("[D]elete Song");
                Console.WriteLine("[V]iew All Songs Playlist");
                Console.WriteLine("E[x]it");

                string choice = Console.ReadLine();
                switch (choice.ToUpper())
                {
                    case "G":
                        {
                            return SongMenu.Select;
                        }
                    case "A":
                        {
                            return SongMenu.Insert;
                        }
                    case "U":
                        {
                            return SongMenu.Update;
                        }
                    case "D":
                        {
                            return SongMenu.Delete;

                        }
                    case "V":
                        {
                            return SongMenu.View;
                        }
                    case "X":
                        {
                            return SongMenu.Exit;
                        }
                    default:
                        {
                            Console.WriteLine("Invalid choice.");
                            Console.ReadKey(true);
                            break;
                        }
                }
            }
        }


        private void Add()
        {
           
            Console.Clear();

            Song song = new Song();

            try
            {
                

                PairPlaylistSongRepository songPlaylist = new PairPlaylistSongRepository(@"../../DataFiles/PairPlaylistSong.txt");
                Insert();





                int SongId = GetRepository().GetNextId() - 1;

             
                if (songPlaylist.GetPlayListSongId(1, SongId) != null)
                {

                   
                    Console.ReadKey(true);
                    return;

                }
                PlayListSongs playListSong = new PlayListSongs();
                playListSong.PlayListId = 1;
                playListSong.SongId = SongId;





                songPlaylist.Save(playListSong);

             
       
            }
            catch (Exception)
            {
                Console.WriteLine("Enter valid data ! ");
                Console.ReadKey(true);
                return;
            }
        }

        private void DeleteFromPlayLists()
        {
            SongRepository songsRepository = new SongRepository(@"../../DataFiles/Songs.txt");
            PairPlaylistSongRepository songPlaylist = new PairPlaylistSongRepository(@"../../DataFiles/PairPlaylistSong.txt");
            Console.Clear();

            try
            {

                int songsId = Delete();
                Console.WriteLine(songsId);

                foreach (PlayListSongs pl in songPlaylist.GetSondId(songsId))
                {
                    songPlaylist.Delete(pl);
                }

                Console.ReadKey(true);

                Console.WriteLine("Songs deleted successfully.");

                Console.ReadKey(true);

            }
            catch (Exception)
            {

                return;
            }
        }
    }
}
