﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlackSoundNew.Entities;
using BlackSoundNew.Repositories;
using BlackSoundNew.Tools;
using BlackSoundNew.Services;

namespace BlackSoundNew.ViewControllers
{
    class SinglePlayListView

    {

        private PlayList entity = null;
        public SinglePlayListView(PlayList entity)
        {
            this.entity = entity;
        }

        public void Show()
        {

            while (true)
            {
                SinglePlayListMenu choice = RenderMenu();

                switch (choice)
                {

                    case SinglePlayListMenu.Insert:
                        {
                            Add();
                            break;
                        }

                    case SinglePlayListMenu.Share:
                        {
                            Share();
                            break;
                        }
                    case SinglePlayListMenu.Delete:
                        {
                            Delete();
                            break;
                        }
                    case SinglePlayListMenu.Exit:
                        {
                            return;
                        }
                }
            }
        }
        private SinglePlayListMenu RenderMenu()
        {
            while (true)
            {

                PairPlaylistSongRepository rep = new PairPlaylistSongRepository(@"../../DataFiles/PairPlaylistSong.txt");
                SongRepository songs = new SongRepository(@"../../DataFiles/Songs.txt");

                Console.Clear();
                Console.WriteLine("---------ALL SONGS:------------------");
                foreach (PlayListSongs pl in rep.GetSongPlaylistID(1))
                {

                    Song song = songs.GetById(pl.SongId);
                    
                    Console.WriteLine("--------------------------------------------");
                    Console.WriteLine("   ID:  " + song.Id);
                    Console.WriteLine("   Title:  " + song.Title);
                    Console.WriteLine("   Artists:  " + song.ArtistName);
                    Console.WriteLine("   Year:  " + song.Year);
                    Console.WriteLine("----------------------------------------------");
                }

                Console.WriteLine("");
                Console.WriteLine("");
                Console.WriteLine("-------------------PLAYLIST-------------------");
                Console.WriteLine("ID:" + entity.Id);
                Console.WriteLine("Name :" + entity.Name);
                Console.WriteLine("Description:" + entity.Description);
                Console.WriteLine("IsPublic?:" + entity.IsPublic);
                Console.WriteLine("-------------SONGS IN THIS PLAYLIST---------");
                Console.WriteLine("");
                foreach (PlayListSongs pl in rep.GetSongPlaylistID(entity.Id))
                {
                    Song song = songs.GetById(pl.SongId);
                    Console.WriteLine("ID:" + song.Id);
                    Console.WriteLine("Title :" + song.Title);
                    Console.WriteLine("Artists:" + song.ArtistName);
                    Console.WriteLine("Year:" + song.Year);
                    Console.WriteLine("");
                    Console.WriteLine("########################################");
                    Console.WriteLine("");
                }
                Console.WriteLine("");
                Console.WriteLine("");
                Console.WriteLine("[A]dd Song to PlayList");
                Console.WriteLine("[S]are playlist");
                Console.WriteLine("[D]edete song from playlist");
                Console.WriteLine("E[X]it");

                string choice = Console.ReadLine();
                switch (choice.ToUpper())
                {

                    case "A":
                        {
                            return SinglePlayListMenu.Insert;
                        }

                    case "S":
                        {
                            return SinglePlayListMenu.Share;
                        }
                    case "D":
                        {
                            return SinglePlayListMenu.Delete;
                        }
                    case "X":
                        {
                            return SinglePlayListMenu.Exit;
                        }

                    default:
                        {
                            Console.WriteLine("Invalid choice.");
                            Console.ReadKey(true);
                            break;
                        }
                }
            }



        }

        public void ShowAllSongsPl()
        {

            PairPlaylistSongRepository rep = new PairPlaylistSongRepository(@"../../DataFiles/PairPlaylistSong.txt");
            SongRepository songs = new SongRepository(@"../../DataFiles/Songs.txt");


            Console.WriteLine("ID:" + entity.Id);
            Console.WriteLine("Name :" + entity.Name);
            Console.WriteLine("Description:" + entity.Description);
            Console.WriteLine("IsPublic?:" + entity.IsPublic);
            Console.WriteLine("########################################");

            foreach (PlayListSongs pl in rep.GetSongPlaylistID(entity.Id))
            {
                Song song = songs.GetById(pl.SongId);
                Console.WriteLine("ID:" + song.Id);
                Console.WriteLine("Title :" + song.Title);
                Console.WriteLine("Artists:" + song.ArtistName);
                Console.WriteLine("Year:" + song.Year);
                Console.WriteLine("########################################");
            }
        

            SharedPlaylistRepository sharedPlaylist = new SharedPlaylistRepository(@"../../DataFiles/SharedPlayLists.txt");
            if (entity.CreatedByUserId == AuthenticationService.LoggedUser.Id || sharedPlaylist.GetPlayListUserId(entity.Id, AuthenticationService.LoggedUser.Id) != null)
            {
                Show();
              
            }
        }
        private void Delete()
        {
            PairPlaylistSongRepository songPlaylist = new PairPlaylistSongRepository(@"../../DataFiles/PairPlaylistSong.txt");

            try { 
            Console.WriteLine("Delete Song:");


            Console.Write("Song ID: ");
            int SongId = int.Parse(Console.ReadLine());
            PlayListSongs playListSong = songPlaylist.GetPlayListSongId(entity.Id, SongId);
            if (playListSong == null)
            {
                Console.WriteLine("Song is not in the playlist");
                Console.ReadKey(true);
                return;
            }
            songPlaylist.Delete(playListSong);

            Console.WriteLine("Song Deleted successfully.");
            Console.ReadKey(true);
            }
            catch (Exception)
            {
                Console.WriteLine("Enter valid data ! ");
                Console.ReadKey(true);
                return;
            }
        }



        private void Add()
        {
            Console.Clear();

            SongRepository songRepository = new SongRepository(@"../../DataFiles/Songs.txt");
            PairPlaylistSongRepository songPlaylist = new PairPlaylistSongRepository(@"../../DataFiles/PairPlaylistSong.txt");

            try { 
            Console.WriteLine("Add Song:");


            Console.Write("Song ID: ");
            int SongId = int.Parse(Console.ReadLine());
            Song song = songRepository.GetById(SongId);

            if (songPlaylist.GetPlayListSongId(entity.Id, SongId) != null)
            {

                Console.WriteLine("Song Already in the playlist");
                Console.ReadKey(true);
                return;

            }

            

            else if (song == null)
            {

                Console.WriteLine("Song not found!");
                Console.ReadKey(true);
                return;
            }



            PlayListSongs playListSong = new PlayListSongs();
            playListSong.PlayListId = entity.Id;
            playListSong.SongId = SongId;





            songPlaylist.Save(playListSong);

            Console.WriteLine("Song Added successfully.");
            Console.ReadKey(true);
            }
            catch (Exception)
            {
                Console.WriteLine("Enter valid data ! ");
                Console.ReadKey(true);
                return;
            }
        } 

        private void Share()
        {
            Console.Clear();

            SharedPlaylistRepository sharedPlaylist = new SharedPlaylistRepository(@"../../DataFiles/SharedPlayLists.txt");
            PairPlaylistSongRepository songPlaylist = new PairPlaylistSongRepository(@"../../DataFiles/PairPlaylistSong.txt");


            Console.WriteLine("Share PlayList:");

            try { 
            Console.Write("User ID: ");
            int UserId = int.Parse(Console.ReadLine());


            if (sharedPlaylist.GetPlayListUserId(entity.Id, UserId) != null)
            {

                Console.WriteLine("PlayList is Alredy Shared");
                Console.ReadKey(true);
                return;

            }
         
            SharedPlaylist sharedPlayList = new SharedPlaylist();

            sharedPlayList.PlayListId = entity.Id;
            sharedPlayList.UserId = UserId;





            sharedPlaylist.Save(sharedPlayList);


            Console.WriteLine("PlayList Shared Succesfuly");
            Console.ReadKey(true);
            }
            catch (Exception)
            {
                Console.WriteLine("Enter valid data ! ");
                Console.ReadKey(true);
                return;
            }
        }




    }
}
