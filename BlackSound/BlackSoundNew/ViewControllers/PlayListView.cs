﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlackSoundNew.Entities;
using BlackSoundNew.Repositories;
using BlackSoundNew.Tools;
using BlackSoundNew.Services;

namespace BlackSoundNew.ViewControllers
{
    class PlayListView:BaseView<PlayList>
        
    {

        public PlayListView(List<PlayList> items) : base(items)
        {

        }

        public override BaseRepository<PlayList> GetRepository()
        {
            return new PlayListRepository(@"../../DataFiles/PlayLists.txt");
        }
        public void Show()
        {
            while (true)
            {
                PlayListMenu choice = RenderMenu();

                switch (choice)
                {
                    case PlayListMenu.Select:
                        {
                           GetAll();
                            break;
                        }
                    case PlayListMenu.Insert:
                        {
                            Add();
                            break;
                        }
                    case PlayListMenu.View:
                        {
                            View();
                            break;
                        }
                    case PlayListMenu.Update:
                        {
                            Update();
                            break;
                        }
                    case PlayListMenu.Delete:
                        {
                            Delete();
                            break;
                        }
                    case PlayListMenu.Exit:
                        {
                            return;
                        }
                }
            }
        }

        private PlayListMenu RenderMenu()
        {
            while (true)
            {
                Console.Clear();

                Console.WriteLine("##########################");
                Console.WriteLine("[G]et all PlayLists");
                Console.WriteLine("[A]dd PlayList");
                Console.WriteLine("[V]iew Playlist");
                Console.WriteLine("[U]pdate PlayList");
                Console.WriteLine("[D]elete PlayList");
                Console.WriteLine("E[x]it");

                string choice = Console.ReadLine();
                switch (choice.ToUpper())
                {
                    case "G":
                        {
                            return PlayListMenu.Select;
                        }
                    case "A":
                        {
                            return PlayListMenu.Insert;
                        }
                    case "V":
                        {
                            return PlayListMenu.View;
                        }
                    case "U":
                        {
                            return PlayListMenu.Update;
                        }
                    case "D":
                        {
                            return PlayListMenu.Delete;
                        }
                    case "X":
                        {
                            return PlayListMenu.Exit;
                        }
                    default:
                        {
                            Console.WriteLine("Invalid choice.");
                            Console.ReadKey(true);
                            break;
                        }
                }
            }
        }

        private void GetAll()
        {
            Console.Clear();

            PlayListRepository playlistRepository = new PlayListRepository(@"../../DataFiles/PlayLists.txt");
            SharedPlaylistRepository sharedPl = new SharedPlaylistRepository(@"../../DataFiles/SharedPlayLists.txt");

           List<PlayList> playlists = playlistRepository.GetAll(AuthenticationService.LoggedUser.Id);
         


            foreach (PlayList playlist in playlists)
            {
                Console.WriteLine("ID:" + playlist.Id);
                Console.WriteLine("Name :" + playlist.Name);
                Console.WriteLine("Description:" + playlist.Description);
                Console.WriteLine("IsPublic?:" + playlist.IsPublic);
                Console.WriteLine("########################################");
            }

            foreach (SharedPlaylist pl in sharedPl.GetUserId(AuthenticationService.LoggedUser.Id))
            {
                PlayList pll = playlistRepository.GetById(pl.PlayListId);
                Console.WriteLine("ID:" + pll.Id);
                Console.WriteLine("Title :" + pll.Name);
                Console.WriteLine("Artists:" + pll.Description);
                Console.WriteLine("Year:" + pll.IsPublic);
                Console.WriteLine("########################################");
            }


            Console.ReadKey(true);
        }

        private void Add()
        {
            Console.Clear();

            PlayList playlist = new PlayList();

            try
            {
                Console.WriteLine("Add new PlayList:");
                Console.Write("Name: ");
                playlist.Name = Console.ReadLine();
                Console.WriteLine("Description");
                playlist.Description = Console.ReadLine();
                Console.WriteLine("Is is public");
                playlist.IsPublic = bool.Parse(Console.ReadLine());
                playlist.CreatedByUserId = AuthenticationService.LoggedUser.Id;

                PlayListRepository playlistRepository = new PlayListRepository(@"../../DataFiles/PlayLists.txt");
                playlistRepository.Save(playlist);


                Console.WriteLine("PL saved successfully.");
                Console.ReadKey(true);
            }
            catch (Exception)
            {
                Console.WriteLine("Enter valid data ! ");
                Console.ReadKey(true);
                return;
            }
        }

        private void Update()

        {
            Console.Clear();
            try
            {
                Console.Write("PlayList ID: ");
                int playlistId = Convert.ToInt32(Console.ReadLine());

                PlayListRepository playlistRepository = new PlayListRepository(@"../../DataFiles/PlayLists.txt");
                PlayList playlist = playlistRepository.GetById(playlistId);

                if (playlist == null || playlist.CreatedByUserId != AuthenticationService.LoggedUser.Id)
                {
                    Console.Clear();
                    Console.WriteLine("PlayList not found.");
                    Console.ReadKey(true);
                    return;
                }

                Console.WriteLine("ID:" + playlist.Id);
                Console.WriteLine("Name (" + playlist.Name + ")");
                Console.WriteLine("New Playlist Name: ");
                string name = Console.ReadLine();

                Console.WriteLine("Description :" + playlist.Description);
                Console.WriteLine("New Playlist Description:  ");
                string description = Console.ReadLine();

                Console.WriteLine("Is it public:  " + playlist.IsPublic);
                Console.Write("New Is it public ?:  ");
                bool ispublic = bool.Parse(Console.ReadLine());


                if (!string.IsNullOrEmpty(name))
                    playlist.Name = name;
                if (!string.IsNullOrEmpty(description))
                    playlist.Description = description;
                if (ispublic != null)
                {
                    playlist.IsPublic = ispublic;
                }

                playlistRepository.Save(playlist);

                Console.WriteLine("PlayList saved successfully.");
                Console.ReadKey(true);
            }
            catch (Exception)
            {
                Console.WriteLine("Enter valid data ! ");
                Console.ReadKey(true);
                return;
            }

        }
        private void Delete()
        {
            PlayListRepository playlistRepository = new PlayListRepository(@"../../DataFiles/PlayLists.txt");
     
            Console.Clear();
            try
            {
                Console.WriteLine("Delete PlayList:");
                Console.Write("Playlist Id: ");
                int playlistId = Convert.ToInt32(Console.ReadLine());

                PlayList playlist = playlistRepository.GetById(playlistId);
                if (playlist == null || playlist.CreatedByUserId != AuthenticationService.LoggedUser.Id)
                {
                    Console.WriteLine("PlayList not found!");
                }
                else
                {
                    playlistRepository.Delete(playlist);

                    Console.WriteLine("PlayList deleted successfully.");

                   
                }
                Console.ReadKey(true);
            }
            catch (Exception)
            {
                Console.WriteLine("Enter valid data ! ");
                Console.ReadKey(true);
                return;
            }
        }

        private void View()

        {
            Console.WriteLine("PlayList Id:");
            int playlistId = int.Parse(Console.ReadLine());

            PlayListRepository playlists = new PlayListRepository(@"../../DataFiles/PlayLists.txt");
            PlayList entity = playlists.GetById(playlistId);

            SharedPlaylistRepository sharedPlaylist = new SharedPlaylistRepository(@"../../DataFiles/SharedPlayLists.txt");

            //if (entity.CreatedByUserId != AuthenticationService.LoggedUser.Id || entity.IsPublic == false)
            //{
            //    Console.WriteLine("This PlayList is Not Yours");
            //    Console.ReadKey(true);
            //    return;
            //}





            if (entity == null)
            {
                Console.WriteLine("Playlist Not Found");
                Console.ReadKey(true);
                return;
            }

            else if (entity.CreatedByUserId == AuthenticationService.LoggedUser.Id || sharedPlaylist.GetPlayListUserId(entity.Id, AuthenticationService.LoggedUser.Id) != null || entity.IsPublic)
            {
                SinglePlayListView viewallPl = new SinglePlayListView(entity);
                viewallPl.ShowAllSongsPl();
                Console.ReadKey(true);


            }

            else
            {
                Console.WriteLine("You don't have rihts for this playlist");
                Console.ReadKey(true);
            }

        }
    }
}
