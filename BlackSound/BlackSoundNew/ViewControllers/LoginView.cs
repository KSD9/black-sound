﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlackSoundNew.Services;
using BlackSoundNew.Repositories;
using BlackSoundNew.Entities;

namespace BlackSoundNew.ViewControllers
{
    class LoginView
    {
        public void Show()
        {
            while (true)
            {
                Console.Clear();
                try
                {
                    Console.Write("Username: ");
                    string username = Console.ReadLine();

                    Console.Write("Password: ");
                    string password = Console.ReadLine();

                    AuthenticationService.AuthenticateUser(username, password);

                    if (AuthenticationService.LoggedUser != null)
                    {
                        Console.WriteLine("Welcome " + AuthenticationService.LoggedUser.Email);
                        Console.ReadKey(true);


                        if (AuthenticationService.LoggedUser.IsAdministrator == true)
                        {
                            SongRepository songRepository = new SongRepository(@"../../DataFiles/Songs.txt");

                            List<Song> songs = songRepository.GetAll();
                            SongView sv = new SongView(songs);
                            sv.Show();
                            break;
                        }

                        else if (AuthenticationService.LoggedUser.IsAdministrator == false)
                        {
                            PlayListRepository playlistrepo = new PlayListRepository(@"../../DataFiles/PlayLists.txt");
                            List<PlayList> playlists = playlistrepo.GetAll();
                            PlayListView pl = new PlayListView(playlists);
                            pl.Show();
                            break;
                        }
                    }
                    else
                    {
                        Console.WriteLine("Invalid username or password");
                        Console.ReadKey(true);
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("Enter valid data ! ");
                    Console.ReadKey(true);
                    return;
                }
            }
        }

    }
}
