﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSoundNew.Entities
{
    class PlayList:BaseEntity
    {

        public string Name { get; set; }
        public string Description { get; set; }
         public bool IsPublic { get; set; }
        public int CreatedByUserId { get; set; }
    }
}
