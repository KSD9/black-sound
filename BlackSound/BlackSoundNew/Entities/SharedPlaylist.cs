﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSoundNew.Entities
{
    class SharedPlaylist: BaseEntity
    {
        public int PlayListId { get; set; }
        public int UserId { get; set; }
    }
}
